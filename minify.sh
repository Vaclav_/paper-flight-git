OUT_PATH=./release/
NODE_APP=node
echo $OUT_PATH
mkdir -p $OUT_PATH
cp -r ./project/* $OUT_PATH
$NODE_APP node_modules/minifier --template {{filename}}.{{ext}} "./release/js"
$NODE_APP node_modules/minifier --template {{filename}}.{{ext}} "./release/lib"