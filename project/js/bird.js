
var Birds  = {
	ar: [],
	min_alt: 15,
	max_alt: 55,
	min_vel: 1.5,
	max_vel: 5,
	miny: 25,
	maxy: Game.HEIGHT-25,
	init: function() {

		var spawn_n = 45-(Level.spawn_interval/250);

		var i;

		for( i=0; i<spawn_n; i++) {
			var vel = Crafty.math.randomInt( this.min_vel, this.max_vel);
			var sy = Crafty.math.randomInt( this.miny, this.maxy);
			var sx = Crafty.math.randomInt( this.miny, Game.WIDTH-this.miny);
			var alt = Crafty.math.randomInt( this.min_alt, this.max_alt);
			//var ang = 3.14/2;

			this.spawnXY( sx, sy, vel, alt);
		}	
	},
	spawnXY: function( x, y, vel, alt) {

		sx = Game.WIDTH+10*Origin.getScale();

		var ang = 3.14/2;

		var p = Crafty.e("Pigeon")
			.setPos( x, y, ang, alt);

		p.body.SetLinearVelocity( new b2Vec2( vel, 0) );
		p.fwd_speed = vel;

		this.ar.push(p);
	},
	spawn: function() {
		//var side = Crafty.math.randomInt(0,1);
		var vel = Crafty.math.randomInt( this.min_vel, this.max_vel);
		var sy = Crafty.math.randomInt( this.miny, this.maxy);
		var alt = Crafty.math.randomInt( this.min_alt, this.max_alt);
		var ang = 3.14/2;
		var sx;
		///if(1) {//side
			sx = -10*Origin.getScale();
		//} else {
		//	sx = Game.WIDTH+10*Origin.getScale();
		//	vel *= -1;
		//	ang *= -1;
		//}

		var p = Crafty.e("Pigeon")
			.setPos( sx, sy, ang, alt);

			p.body.SetLinearVelocity( new b2Vec2( vel, 0) );
			p.fwd_speed = vel;

			this.ar.push(p);

	}
};

Crafty.c("Pigeon", {

	type: "bird",

	fwd_speed: 0,
	falling: false,

	init: function() {
		this.addComponent("2D, Canvas, PigeonSprite, Box2D");
	},
	setPos: function( px, py, ang, alt) {
		var pos = new b2Vec2(px, py);
		
		this.attr( {x: pos.x, y: pos.y});

		this.orig_w = this.w;
		this.orig_h = this.h;

		this.box2d( {
			alt: alt,
			bodyType: 'dynamic',
			density : 0.01,
			friction : 0.1,
			restitution : 0.1,
			//linearDamping: 0.2,
			angularDamping: 2
		});
		this.body.type = this.type;

		this.body.SetAngle(ang);

		return this;
	},
	update: function() {
		//Add impulse forward to preserve speed
		/*
		var dvel;
		if(this.fwd_speed > 0)
	    	dvel = this.fwd_speed - this.body.GetLinearVelocity().Length();
	    else
	    	dvel = this.fwd_speed + this.body.GetLinearVelocity().Length();
	    var force_mag = this.body.GetMass() * dvel / (1/50.0); //f = mv/t
	    var fwd_vect = this.body.GetWorldVector( new b2Vec2( 0.0, -1.0) );
	    var force = new b2Vec2( fwd_vect.x*force_mag, fwd_vect.y*force_mag);
    	this.body.ApplyForce( force, this.body.GetWorldCenter() );
    	*/
	}
});