
var UI = {
	BASE_Z: 1001,
	HEIGHT: 60,
	iconh: 48,
	iconw: 36,
	lifex: 30,
	lifey: -54,

	Life: null,

	init: function() {

		$("#Navbar").css( "zIndex", this.BASE_Z);
		$("#Life").css( "zIndex", this.BASE_Z);
		$("#LifeCnt").css( "zIndex", this.BASE_Z);
		$("#Time").css( "zIndex", this.BASE_Z);
		$("#Score").css( "zIndex", this.BASE_Z);
		$("#Planes").css( "zIndex", this.BASE_Z);

		$("#EndText").css( "zIndex", -1);

		this.updateScore();
		this.updateTime();
		this.updatePlanesCount();
		this.updateLife();
	},
	endText: function(msg) {
		$("#EndText").html(msg);
		$("#EndText").css( "zIndex", this.BASE_Z);

		var scale = Origin.getScale();
		if( Origin.VIEWPORT_ON)
			basex = (Crafty.viewport.width-Game.WIDTH*Origin.getScale())/2;
		else
			basex = 0;
		var x = basex + Game.WIDTH/2*scale -150;
		var y = UI.HEIGHT + Game.HEIGHT/2*scale - 40;
		$("#EndText").css({ left: x+"px", top: y+"px", background: "#63b4fb"}); 
	},
	hideEndText: function() {
		$("#EndText").html("");
		$("#EndText").css( "zIndex", -1);
	},
	addScore: function(){
		Level.score++;
		Level.life++;
		this.updateScore();
		this.updateLife();

		if(Level.score >= Level.target)
			Level.win();
	},
	updateLife: function() {
		$("#LifeCnt").html(Level.life);
		if( Level.life <= 0 && P1Planes.length === 0)
			Level.lose();
	},
	updateScore: function() {
		$("#Score").html("Score: " + Level.score + " / " + Level.target);
	},
	updateTime: function() {
		var min = Math.floor(Level.time/60.0);
		var sec = Level.time%60;
		if( sec < 10 )
			sec = "0"+sec;
		$("#Time").html("Time: " + min + ":" + sec );
	},
	updatePlanesCount: function() {
		var text = "";
		for( var i in P1Planes)
			text += ". ";
		$("#Planes").html(text);
		if( Level.life <= 0 && P1Planes.length === 0)
			Level.lose();
	},
	reScale: function() {
		var scale = Origin.getScale();

		var basex;
		if( Origin.VIEWPORT_ON)
			basex = (Crafty.viewport.width-Game.WIDTH*Origin.getScale())/2;
		else
			basex = 0;

		var lifex = 30*scale;
		var iconx = basex + lifex;
		$("#Life").css({left: iconx, position: 'absolute'});

		var life_cntx = lifex+45;
		life_cntx += basex;
		$("#LifeCnt").css({left: life_cntx, position: 'absolute'});

		var timex = 750*scale;
		timex += basex;
		$("#Time").css({left: timex, position: 'absolute'});

		var scorex = 550*scale;
		scorex += basex;
		$("#Score").css({left: scorex, position: 'absolute'});

		var planesx = life_cntx+45;
		
		var planesw = 490*scale-90;//this.PLANES_W*scale;
		//planesx += basex;
		$("#Planes").css({left: planesx, position: 'absolute', width: planesw});	

		var x = basex + Game.WIDTH/2*scale -150;
		var y = UI.HEIGHT + Game.HEIGHT/2*scale - 40;
		$("#EndText").css({ left: x+"px", top: y+"px", background: "#63b4fb"}); 
	}
};