
var Level = {

	MAX_LEVEL: 7,

	Lives: [ 1, 5, 12, 20, 30, 12, 12],
	Times: [ 300, 120, 120, 120, 220, 90, 60],
	Targets: [ 1, 4, 15, 10, 40, 8, 8],
	Spawns: [ 2000, 1500, 1000, 1000, 750, 500, 500],

	won: false,
	lost: false,

    life: 20,
    score: 0,
    time: 2,
    current: 0,
    target: 1,
	spawn_interval: 2000,

	nextLevel: function() {
		this.won = false;
		this.lost = false;

		location.reload(false);
	},

	isOver: function() {
		return ( this.won || this.lost);
	},

	win: function() {
		if( this.won || this.lost)
			return;
		this.won = true;

		var last_level;
		if( this.current >= this.MAX_LEVEL-1)
			last_level = " (Last level)";
		else
			last_level = "";

		UI.endText("<span class='green'>Level "+ (this.current+1) +" won!" + last_level + "</span><br/>Press enter to continue.");

		this.current++;

		this.waitNext();
	},
	lose: function() {
		if( this.won || this.lost)
			return;
		this.lost = true;
		
		UI.endText("<span class='red'>Level lost.</span><br/>Press enter to restart.");

		//restart level

		this.waitNext();
	},
	startLevel: function() {
		var n = this.current;
		if(n>=this.MAX_LEVEL)
			n = this.MAX_LEVEL-1;
		this.life = this.Lives[n];
		this.score = 0;
		this.time = this.Times[n];
		this.target = this.Targets[n];
		this.spawn_interval = this.Spawns[n]*3;
	},
	waitNext: function() {
		var self = this;
		Crafty.e("Keyboard")
			.bind("KeyDown", function() {
				if( this.isDown('ENTER') ) {
					this.destroy();
					Crafty.storage("level", self.current);
					self.nextLevel();
				}
			});
	}
};