
var Game = {
    SKIP_LOGO: false,
    AUDIO: true,

    version: "1.01",

    WIDTH: 900,
    HEIGHT: 600,

    Z_LOADING: 100,

    debug_draw: false,

    started: false,

    player_won: false,
    player_lost: false,

    volume: 1.0,
};

//// Loading config
var scripts = [
    "lib/Box2dWeb-2.1.a.3.js",
    "lib/crafty_box2d.js",
    "lib/jquery-2.1.3.min.js"
];

var Assets = {
    "images": [
        "img/logo.png",
        "img/buildings.jpg",
        ],
    "sprites": {
        "img/plane.png": {
            "tile": 80,
            "tileh": 126,
            "map": { "PlaneSprite": [0,0] }
        },
        "img/plane_fall.png": {
            "tile": 80,
            "tileh": 126,
            "map": { "PlaneFalling": [0,0] }
        },
        "img/pigeon.png": {
            "tile": 292,
            "tileh": 158,
            "map": { "PigeonSprite": [0,0] }
        },
        "img/window.png": {
            "tile": 25,
            "tileh": 12,
            "map": { "WindowSprite": [0,0] }
        },
        "img/life.png": {
            "tile": 36,
            "tileh": 48,
            "map": { "LifeDefault": [0,0], "LifeHover": [1,0] }
        },
        "img/check.png": {
            "tile": 48,
            "tileh": 48,
            "map": { "CheckSprite": [0,0], "CrossSprite": [1,0] }
        }
    }
};

var AudioAssets = {
        theme: [ "audio/ambient_6.mp3", "audio/ambient_6.ogg"],
    };

////End of Loading config


function getUrlVariables() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

var Loader =  {
    //Variables used internally
    scripts_loaded: 0,
    assets_total: 0,
    //Configuration
    RELOAD_SCRIPTS: true,
    //Loading bar settings
    loading_hold_time: 500,     //Time before the loading bar, and the logo background dissapears
    bar_x: Crafty.viewport.width/2,
    bar_y: Crafty.viewport.height/2,
    bar_width_p: 40,    //%
    bar_height: 5,     //px
    bar_border: 1,      //px
    bar_bg_color: "black",
    bar_fg_color: "yellow",
    bar_fg_entity: null,   //Entity
    bar_text: null,
    //Loading screen
    ld_bg_color: "#EEEEEE",
    logo_bg_color: "#63b4fb",
    logo_img: "img/logo.png",
    logo_x: Crafty.viewport.width/2,
    logo_y: Crafty.viewport.height/2,

    logo_time: 1000,
    logo_fade_time: 1000,

    tween_finished: false,

    sprites: [],

    //Loading screen
    updateScreen: function( scripts_complete, assets_complete) {
        var total = scripts.length + this.assets_total;
        var percent = 0;
        if( this.assets_total !== 0)
            percent = (scripts_complete + assets_complete)/total;
        if( percent > 1.0 ) percent = 1.0;
        
        var new_w = Crafty.viewport.width*this.bar_width_p/100*percent;
        this.bar_fg_entity.attr({ w: new_w });

        this.bar_text.text("Loading... " + Math.round(percent*100) + "%");

        //console.log("p: " + percent);
        
    },

    //Start loading
    start: function() {

        //Update variables
        this.bar_x = Crafty.viewport.width/2;
        this.bar_y = Crafty.viewport.height/2;
        this.logo_x = Crafty.viewport.width/2;
        this.logo_y = Crafty.viewport.height/2;

        //Set up loading screen
        var bar_z = Game.Z_LOADING+1;
        var bar_width_px = Crafty.viewport.width*this.bar_width_p/100;

        var ld_bg = Crafty.e("2D, DOM, Color")
            .attr({ x: 0, y: 0, w: Crafty.viewport.width, h: Crafty.viewport.height})
            .color( this.ld_bg_color );

        this.sprites.push(ld_bg);
        
        var bar_bg = Crafty.e("2D, DOM, Color")
            .attr({ 
                x: this.bar_x-bar_width_px/2-this.bar_border,
                y: this.bar_y-this.bar_height/2-this.bar_border,
                w: bar_width_px+this.bar_border*2,
                h: this.bar_height+this.bar_border*2,
                z: bar_z
            })
            .color( this.bar_bg_color );
    
        this.sprites.push(bar_bg);

        var text_w = 200;
        var text_h = 25;
        this.bar_text = Crafty.e("2D, DOM, Text")
            .attr({ x: this.bar_x-text_w/2, y: this.bar_y-text_h, w: text_w, h: text_h, z: bar_z })
            .text("Loading... 0%")
            .textFont( { size: "16px"} )
            .css( { "text-align": "center" } );

        this.sprites.push(this.bar_text);

        this.bar_fg_entity = Crafty.e("2D, DOM, Color, Tween")
            .attr({
                x: this.bar_x-bar_width_px/2,
                y: this.bar_y-this.bar_height/2,
                w: bar_width_px,
                h: this.bar_height,
                z: bar_z
            })
            .color( this.bar_fg_color );

        this.sprites.push(this.bar_fg_entity);

        //Start loading
        this.loadAssets();
    },

    loadAssets: function() {
        var self = this;

        //Load Audio, images, sprites
        Crafty.load( Assets,
            function() {    //Loaded
                console.log("Assets done");
                self.loadScript();
            },
            function(e) {   //Progress
                console.log("Progress: " + e.loaded + " total: " + e.total + " percent: " + e.percent);
                if( self.assets_total === 0)
                    self.assets_total = e.total;
                self.updateScreen( 0, e.loaded);
            },
            function(e) {   //Error
                console.log("Loading error: " + JSON.stringify(e));
                if( self.assets_total === 0)
                    self.assets_total = e.total;
                self.updateScreen( 0, e.loaded);
            }
        );
    },

    loadScript: function() {
        var script = document.createElement('script');

        //get next script
        script.src = scripts[ this.scripts_loaded];

        if( this.RELOAD_SCRIPTS ) { //date: disable caching
            var now = new Date();
    	    script.src += "?nocache=" + now.getTime();
        }
        
	    script.setAttribute('charset', 'utf-8');

        var self = this;        

        if( this.scripts_loaded >= scripts.length ) {
            if( Game.SKIP_LOGO )
                this.finished();
            else
                setTimeout( function() { self.finished(); }, this.loading_hold_time );
        } else {
            //script.onload
            script.onload = function() {
                self.scripts_loaded++;
                console.log("scripts: " + self.scripts_loaded);

                self.updateScreen( self.scripts_loaded, self.assets_total);

                //Finished loading all scripts
                if( self.scripts_loaded >= scripts.length ) {
                    if( Game.SKIP_LOGO )
                        self.finished();
                    else
                        setTimeout( function() { self.finished(); }, self.loading_hold_time );
                }
                else    //Load next script
                    self.loadScript();
            };
            document.head.appendChild(script);
        }
        
    },

    cleanup: function() {
        for( var i=0; i<this.sprites.length; i++)
            this.sprites[i].destroy();
        
        this.sprites = [];
    },

    //Loading finished, put up logo
    finished: function() {

        if( Game.SKIP_LOGO )
            this.cleanup();
        else {  //Show logo
            var logo_bg = Crafty.e("2D, DOM, Color, Tween, Persist")
                .attr({ x: 0, y: 0, w: Crafty.viewport.width, h: Crafty.viewport.height, z: Game.Z_LOADING-2 })
                .color( this.logo_bg_color );

            var logo = Crafty.e("2D, DOM, Image, Tween, Persist")
                .attr({ z: Game.Z_LOADING-1 })
                .image( this.logo_img, "no-repeat" );

                logo.attr({ x: this.logo_x-logo.w/2, y: this.logo_y-logo.h/2 });

            var logo_version_sprite = Crafty.e("2D, DOM, Text, Tween, Persist")
                .attr({ z: Game.Z_LOADING-1 })
                .text("v"+Game.version)
                .attr({x: logo.x+logo.w/2+Crafty.viewport.width/2*0.5, y: logo.y+logo.h+ Crafty.viewport.height/2*0.2, w: 100, h: 50})
                .textColor("#000000")
                .textFont({ size: "22px", family: "Bfont", weight: "bold"});

            this.cleanup();

            this.sprites.push(logo_bg);
            this.sprites.push(logo);
            this.sprites.push(logo_version_sprite);
        }

        if( !Game.SKIP_LOGO ) {
            var self = this;

            setTimeout( function() {
                //logo_bg.tween( { alpha: 0.0 }, self.logo_fade_time );
                var tween = logo.tween( { alpha: 0.0 }, self.logo_fade_time );
                tween.bind( "TweenEnd", function() {
                        setTimeout( function() {
                            self.cleanup();
                            //Start intro
                            Intro.start();
                        }, self.loading_hold_time
                    );
                });

                logo_version_sprite.tween( { alpha: 0.0}, self.logo_fade_time);

            }, self.logo_time );
            
        }

        console.log("Finished downloading");

        Crafty.scene("Game");
        if( Game.SKIP_LOGO )
            Intro.start();
    }
    
};

//Entry point
window.onload = init();

//Init crafty and call loading screen
function init() {

    var url_obj =  getUrlVariables();
    var sound_conf = url_obj.sound;
//    console.log("sound: "+sound_conf);

    if(sound_conf === "off") {
        Game.AUDIO = false;
    }

    //Makes Crafty go fullscreen even on desktop
    Crafty.mobile = true;
    
    Crafty.init();    //get: Crafty.viewport.width
    Crafty.canvas.init();

    Crafty.background("#EEEEEE");

    if( Game.AUDIO ) {
        Assets.audio = AudioAssets;
    }

    var level = Crafty.storage("level");
    console.log("LEVEL: "+level);
    if( level !== null && level != 0)
        Game.SKIP_LOGO = true;

    Loader.start();
}
