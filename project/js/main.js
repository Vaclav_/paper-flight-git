/*
Controls:
discard side velocity
or: turn velocity?
*/
var dyn_bodies = [];

var P1Planes = [];

var Origin = {
    //If viewport should be scaled and translated to fit window
    VIEWPORT_ON: true,
    w: 900,
    h: 600,
    center_x: 450,
    center_y: 300,
    bg: null,
    PTM_RATIO: 32,
    getScale: function() {
    	if(this.VIEWPORT_ON)
        	return ((Crafty.viewport.height-UI.HEIGHT)/this.bg.h);
    	else
    		return 1.0;
    },
    setViewport: function() {
        if(this.VIEWPORT_ON) {
            var scale = this.getScale();
            Crafty.viewport.scale( scale );
            Crafty.viewport.x = (Crafty.viewport.width - this.bg.w*scale)/2/scale;
            Crafty.viewport.y = UI.HEIGHT/scale;
        }

        UI.reScale();
    },
    altToScale: function( alt ) {
        if( alt < 0.0)
            alt *= (-1);

        if( alt < 0.1)
            return 42.3;
        else	//Magic number form plane default size at x meters and desired size at max altitude
            return (4.23/alt);
    },
    toBox2DCoord: function( vect, alt ) {
        var scale = this.altToScale(alt);
        vect.x = (vect.x - this.center_x)/scale/this.PTM_RATIO;
        vect.y = (vect.y - this.center_y)/scale/this.PTM_RATIO;
    },
    toCraftyCoord: function( vect, alt ) {
        var scale = this.altToScale(alt);
        vect.x = vect.x*scale*this.PTM_RATIO + this.center_x;
        vect.y = vect.y*scale*this.PTM_RATIO + this.center_y;
    }
};

var Intro = {

	cleanup: function() {
	},

	start: function() {

		if( Game.AUDIO )
			Crafty.audio.play("theme", -1, Game.volume);

		var self = this;

		/* //would be good fix but the mouse gets broken 
		if( Origin.VIEWPORT_ON === false ) {
			Crafty.stage.elem.style.position = 'absolute';
			Crafty.stage.elem.style.top = '60px';
		}
		else */
		Origin.setViewport();

		//Crafty.box2D.init( 0, 0, 32, true);
		if(Game.debug_draw)
			Crafty.box2D.initDebugDraw();

		UI.init();

		Crafty.e("Delay").delay(function() {
			Level.time -= 1;
			if( Level.time <= 0) {
				Level.time = 0;
				Level.lose();
			}
			UI.updateTime();
		}, 1000, -1);
	}
};

var TargetPlane = null;

var Mouse = {
	x: 0,
	y: 0,
};

Crafty.scene("Game", function() {

	var level = Crafty.storage("level");
	if(level == null)
		level = 0;
	Level.current = level;
	Level.startLevel();

	Origin.bg = Crafty.e("2D, Canvas, Image")
		.attr({ x: 0, y: 0, z: -10})
		.image("img/buildings.jpg", "no-repeat");

	var mouse_listener = Crafty.e("2D");
	mouse_listener.onMouseMove = function(e) {
		Mouse.x = e.x;
		Mouse.y = e.y;
		//console.log(Mouse);
	};
	mouse_listener.onMouseDown = function(e) {

		Building.deselectPlane();

	};
	Crafty.addEvent( mouse_listener, Crafty.stage.elem, "mousemove", mouse_listener.onMouseMove);

	Crafty.addEvent( mouse_listener, Crafty.stage.elem, "mousedown", mouse_listener.onMouseDown);

	var p = Crafty.e("Pigeon")
		.setPos(0, 250, 3.14/2, 50);

		p.body.SetLinearVelocity( new b2Vec2( 10, 0) );


	var viewport_d = Crafty.viewport.width/Origin.getScale()-Game.WIDTH;
	Crafty.e("2D, DOM, Color")
		.attr({x: -viewport_d, y: 0, w: viewport_d, h: Game.HEIGHT})
		.color("#EEEEEE");
	Crafty.e("2D, DOM, Color")
		.attr({x: Game.WIDTH, y: 0, w: viewport_d, h: Game.HEIGHT})
		.color("#EEEEEE");
	if( Origin.VIEWPORT_ON === false ) {
		Crafty.e("2D, DOM, Color")
			.attr({x: 0, y: Game.HEIGHT, w: Game.WIDTH, h: 50})
			.color("#EEEEEE");
	}

	var win = Crafty.e("2D, Canvas, WindowSprite")
		.attr({ x: 460, y: 422, z: 100-63.0});
		win._globalZ = 100-63.0;

	Birds.init();

	Crafty.e("Delay").delay( function() {
		Birds.spawn();
	}, Level.spawn_interval, -1);

	Crafty.e("Keyboard").bind('KeyDown', function() {
		if((this.isDown('SPACE') || this.isDown('W') || this.isDown('UP_ARROW')) && !Level.isOver()) {
			if( Building.spawn_cd <= 0) {
				Building.spawn_cd = Building.SPAWN_CD_INIT;
				Building.spawnPlane();
				UI.updatePlanesCount()
			}
		}
		if( this.isDown("M")) {
			if(Game.AUDIO)
				Crafty.audio.togglePause("theme");
		}
		if( this.isDown("0")) {
			Crafty.storage("level", 0);
			Level.nextLevel();
		}
		if( this.isDown("T") )
			Building.selectNextPlane();
		else if( this.isDown("R") )
			Building.selectPrevPlane();
	});

	Crafty.bind("EnterFrame", function() {
		var i;

		Building.checkFallenPlanes();

		for( i in P1Planes)
			P1Planes[i].update();

		var bird_deleted = false;
		for( i in Birds.ar) {
			var bird =  Birds.ar[i];
			if( bird.x < -bird.w || bird.x>Game.WIDTH+bird.w ) {
				Crafty.box2D.world.DestroyBody(  bird.body );
				 bird.destroy();
				 for( var k in dyn_bodies) {
				 	if(dyn_bodies[k] == bird.body) {
				 		dyn_bodies.splice(k, 1);
				 		break;
				 	}
				 }
				 Birds.ar[i] = null;
				 bird_deleted = true;
			} else
				Birds.ar[i].update();
		}

		//Rebuild array
		if(bird_deleted) {
			var birds = [];
			for( i in Birds.ar) {
				if( Birds.ar[i] != null )
					birds.push(Birds.ar[i]);
			}
			Birds.ar = birds;
		}
		

		Crafty.trigger("StepWorld");

		if( Building.spawn_cd > 0)
			Building.spawn_cd -= 1;
		
	});

});


window.onresize = function() {
	Origin.setViewport();
};

/*Crafty.c("Circle", {
    Circle: function(radius, color) {
        this.radius = radius;
        this.w = this.h = radius * 2;
        this.color = color || "#000000";
        
        return this;
    },

    ready: true,
    
    draw: function() {
    	Game.WIDTH = Crafty.viewport.width;
    	Game.HEIGHT = Crafty.viewport.height;
       var ctx = Crafty.canvas.context
       ctx.save();
       ctx.strokeStyle = "#FF0000";//this.color;
       ctx.beginPath();
       ctx.moveTo(0,0);
       ctx.lineTo(Game.WIDTH, 0);
       ctx.lineTo(Game.WIDTH, Game.HEIGHT);
       ctx.lineTo(0, Game.HEIGHT);
       ctx.lineTo(0, 0);
       ctx.closePath();
       ctx.stroke();
    }
}); */
