
var Building = {

	MIN_ALT: 15,

	ang_min: -0.52,
	ang_max: 0.52,

	fall_min: 1.75/50.0,
	fall_max: 2.5/50.0,

	SPAWN_CD_INIT: 60,
	spawn_cd: 0,

	deselectPlane: function() {
		if(TargetPlane !== null ) {
			TargetPlane.control_on = false;
			TargetPlane.left_down = false;
			TargetPlane.right_down = false;
			if( TargetPlane.selection_sprite !== null) {
				TargetPlane.selection_sprite.destroy();
				TargetPlane.selection_sprite = null;
			}
		}
		TargetPlane = null;
	},

	selectPrevPlane: function() {
		var LastPlane = TargetPlane;
		this.deselectPlane();

		if(P1Planes.length === 0)
			return;

		var plane_i = 0;

		if( LastPlane !== null ) {
			while( plane_i < P1Planes.length) {
				if( P1Planes[plane_i] == LastPlane)
					break;
				plane_i++;
			}
		}

		if( plane_i === 0 || plane_i >= P1Planes.length )
			plane_i = P1Planes.length-1;
		else
			plane_i--;

		P1Planes[plane_i].control_on = true;
		TargetPlane = P1Planes[plane_i];

	},

	selectNextPlane: function() {
		var LastPlane = TargetPlane;
		this.deselectPlane();

		if(P1Planes.length === 0)
			return;

		var plane_i = 0;

		if( LastPlane !== null ) {
			while( plane_i < P1Planes.length) {
				if( P1Planes[plane_i] == LastPlane)
					break;
				plane_i++;
			}
		}

		if( plane_i >= P1Planes.length-1 )
			plane_i = 0;
		else
			plane_i++;

		P1Planes[plane_i].control_on = true;
		TargetPlane = P1Planes[plane_i];
	},

	spawnPlane: function() {
		var px = 472;
		var py = 427;

		if(Level.life <= 0)
			return;

		var dir = Crafty.math.randomNumber(this.ang_min, this.ang_max);
		var fall_speed = Crafty.math.randomNumber(this.fall_min, this.fall_max);

		var plane = Crafty.e("Plane");
		plane.setPos( px, py, dir);
		plane.fall_speed = fall_speed;

		P1Planes.push(plane);

		var self = this;
		//select if first plane
		if(P1Planes.length == 1) {
			setTimeout( function() {
				self.selectNextPlane();
			}, 1000);
		}

		Level.life--;
		UI.updateLife();
	},
	checkFallenPlanes: function() {
		var plane;
		var del = false;
		for (var i in P1Planes) {
			plane = P1Planes[i];
			if((plane.x+plane.w) < 0 || (plane.y+plane.h) < 0 || plane.x > Game.WIDTH || plane.y > Game.HEIGHT) {
				var cx = plane.x+plane.w/2;
				var cy = plane.y+plane.h/2;
				var w = 48;
				var h = 48;
				var signx=cx-w/2;
				var signy=cy-h/2;
				if(plane.x+plane.w < 0)
					signx = 0;
				else if( plane.x > Game.WIDTH )
					signx = Game.WIDTH-w;

				if(plane.y+plane.h < 0)
					signy = 0;
				else if( plane.y > Game.HEIGHT)
					signy = Game.HEIGHT-h;

				var sprite_name;

				//alt --> + score? + texture
				if( plane.body.alt <+ Building.MIN_ALT && !plane.hit ) {
					UI.addScore();
					sprite_name = "CheckSprite";
				}
				else
					sprite_name = "CrossSprite";

				Crafty.e("2D, DOM, Delay, Tween, " + sprite_name)
					.attr({ x: signx, y: signy})
					.delay( function() {
						var self = this;
						this.tween({alpha: 0.0}, 1000);
						this.bind("TweenEnd", function() {
							this.destroy();
						});
					}, 1000);

				//Remove selection

				if(TargetPlane === plane)
					Building.deselectPlane();

				 for( var k in dyn_bodies) {
				 	if(dyn_bodies[k] == plane.body) {
				 		dyn_bodies.splice(k, 1);
				 		break;
				 	}
				 }
				Crafty.box2D.world.DestroyBody(plane.body);
				plane.destroy();
				P1Planes[i] = null;
				del = true;
			}
		}

		if(del==false)
			return;

		var planes_new = [];
		for( i in P1Planes) {
			if(P1Planes[i] != null)
				planes_new.push(P1Planes[i]);
		}
		P1Planes = planes_new;

		UI.updatePlanesCount();
	}
};

Crafty.c("Plane", {
	orig_w: 100,
	orig_h: 100,

	type: "plane",

	//if the plane was hit and going down
	hit: false,

	//altitude
	init_alt: 65.0,
	falling: true,
	fall_speed: 2/50.0,
	fall_acc: 0.25/50.0,

	turning: false,
	torque_step: 10,//0.6
	max_ang_vel: 1.75,//5
	control_on: false,

	fwd_speed: 6,

	selection_sprite: null,

	left_down: false,
	right_down: false,

	init: function() {
		this.addComponent("2D, Canvas, Box2D, Keyboard, Mouse, PlaneSprite");
	},
	doHit: function() {
		if(this.hit === false) {
			this.hit = true;
			this.removeComponent("PlaneSprite");
			this.addComponent("PlaneFalling");


			var scale = Origin.altToScale(this.body.alt);
			var new_w = this.orig_w*scale;
			var new_h = this.orig_h*scale;
			var new_z = 100.0-this.body.alt;
			this.attr({w: new_w, h: new_h});
			//workaround for setting .z not working
			this._globalZ = new_z;
			this.origin("center");
		}

	},
	select: function() {

	},
	setPos: function( px, py, ang) {
		var pos = new b2Vec2(px, py);
		
		this.attr( {x: pos.x, y: pos.y});

		this.orig_w = this.w;
		this.orig_h = this.h;

		this.box2d( {
			alt: this.init_alt,
			bodyType: 'dynamic',
			density : 0.01,
			friction : 0.1,
			restitution : 0.1,
			//linearDamping: 0.2,
			angularDamping: 1.5
		});
		this.body.type = this.type;
		this.body.SetAngle(ang);

		this.bind("Click", function() {

			this.control_on = true;
			TargetPlane = this;

		});

		this.bind('KeyDown', function() {

			if(this.control_on) {
				var dw;
				if(this.isDown('LEFT_ARROW') || this.isDown('A') )
					this.left_down = true;
				
				if(this.isDown('RIGHT_ARROW') || this.isDown('D'))
					this.right_down = true;
				
			}

		});

		this.bind('KeyUp', function() {
			if( !this.isDown('LEFT_ARROW') && !this.isDown('A') && this.left_down == true )
				this.left_down = false;

			if( !this.isDown('RIGHT_ARROW') && !this.isDown('D') && this.right_down == true)
				this.right_down = false;

		});

		return this;
	},
	update: function() {

		if( this.hit )
			this.fall_speed += this.fall_acc;

		if(this.control_on) {
			var scale = Origin.altToScale(this.body.alt);
			var _w = 185*scale;
			var _h = 185*scale;
			//Create selection
			 if( this.selection_sprite === null) {
				this.selection_sprite = Crafty.e("2D, Canvas, Color")
					.origin("center")
					.color("#FF0000")
					.attr({ w: _w, h: _h, alpha: 0.3})
					.attr({ x: this.x-_w/2+this.w/2, y: this.y-_h/2+this.h/2, z: 1});

				this.selection_sprite._globalZ = 1;
			//Update selection				
			} else {

				this.selection_sprite.attr({ w: _w, h: _h})
					.attr({ x: this.x-_w/2+this.w/2, y: this.y-_h/2+this.h/2})
					.origin("center")
					._globalZ = 11;
			}

		}



		//Stop going sideways -> the plane will turn
		var vel = this.body.GetLinearVelocity();
		var side_vect = this.body.GetWorldVector( new b2Vec2( 1.0, 0.0) );

		//Get side vel
		var side_vel = b2Math.Dot( side_vect, vel);

		var impulse_vect = new b2Vec2( 0.0, 0.0);
		impulse_vect.x = -side_vect.x * side_vel * this.body.GetMass();	//i = m*v
		impulse_vect.y = -side_vect.y * side_vel * this.body.GetMass();

		this.body.ApplyImpulse(impulse_vect, this.body.GetWorldCenter());

		//Add impulse forward to preserve speed
	    var dvel = this.fwd_speed - this.body.GetLinearVelocity().Length();
	    var force_mag = this.body.GetMass() * dvel / (1/50.0); //f = mv/t
	    var fwd_vect = this.body.GetWorldVector( new b2Vec2( 0.0, -1.0) );
	    var force = new b2Vec2( fwd_vect.x*force_mag, fwd_vect.y*force_mag);
		this.body.ApplyForce( force, this.body.GetWorldCenter() );


		//Apply controls
		if( this.left_down && !this.hit) {
			var dw = -this.torque_step * 1.0/50.0 / this.body.GetInertia();

			if( this.body.GetAngularVelocity()+dw > -this.max_ang_vel)
				this.body.ApplyTorque(-this.torque_step);
			else
				this.body.SetAngularVelocity(-this.max_ang_vel);

		} else if( this.right_down && !this.hit) {
			var dw = this.torque_step * 1.0/50.0 / this.body.GetInertia();

			if( this.body.GetAngularVelocity()+dw < this.max_ang_vel)
				this.body.ApplyTorque(this.torque_step);
			else
				this.body.SetAngularVelocity(this.max_ang_vel);
		}
	},
	scale: function( new_scale ) {
		var new_w = this.orig_w*new_scale;
		var new_h = this.orig_h*new_scale;
		if( new_w != this.w || new_h != this.h ) {

			this.attr({w: new_w, h: new_h});

			this.origin("center");
		}

	},
});